## Exercicis de disseny de bases de dades.

### Habitatges
Dissenyar un esquema E/R que reculli l'organització d'un sistema d'informació en el qual es vol tenir la informació sobre municipis, habitatges i persones. Cada persona només pot habitar en un habitatge, però pot ser propietària de més d'una. També ens interessa la interrelació de les persones amb el seu cap de família.
Feu els supòsits semàntics (`requeriments d'usuari`) complementaris necessaris.
***
### Carreteres
Dissenyar una base de dades que contingui informació relativa a totes les carreteres d'un determinat país. Es demana realitzar el disseny en el model E/R, sabent que:
- En aquest paıs les carreteres es troben dividides en trams.
- Un tram sempre pertany a una única carretera i no pot canviar de carretera.
- Un tram pot passar per diversos termes municipals, sent una dada d'interès al km. del tram pel qual entra en dit terme municipal i al km. pel qual surt.
- Hi ha una sèrie d'àrees en les que s'agrupen els trams, cada un dels quals no pot pertànyer a més d'una àrea.  

Establiu els atributs que considereu oportuns i d'ells, trieu `identificador` per a cada entitat.
***
### Zoos  
Es vol dissenyar una base de dades relacional per emmagatzemar informació relativa als zoos existents en el món, així com les espècies i animals que aquests alberguen.
- De cada zoo es vol emmagatzemar el seu codi, nom, la ciutat i país on es troba, mida (m2) i pressupost anual.
- Cada zoo codifica els seus animals amb un codi propi, de manera que entre zoos es pot donar el cas que es repeteixi.
- De cada espècie animal s'emmagatzema un codi d'espècie, el nom vulgar, el nom científic, família a la qual pertany i si es troba en perill d'extinció.
- A més, s'ha de guardar informació sobre cada animal que els zoos tenen, com el seu número d'identificació, espècie, sexe, any de naixement, país d'origen i continent.

Establiu els atributs que considereu oportuns i d'ells, trieu `identificador` per a cada entitat.
***
### Càtedres d'Universitat
Dissenyar una base de dades que reculli l'organització d'una Universitat. Es considera que:
- Els departaments poden estar en una sola facultat o ser interfacultatius,
agrupant en aquest cas càtedres que pertanyen a facultats diferents.
- Una càtedra es troba en un únic departament.
- Una càtedra pertany a una sola facultat.
- Un professor està sempre assignat a un únic departament i adscrit a una o diverses càtedres, podent canviar de càtedra, però no de departament. Interessa la data en què un professor és adscrit a una càtedra.
- Hi ha àrees de coneixement, i tot departament tindrà una única àrea de coneixement.
***
### Sucursal bancària
Es desitja dissenyar una base de dades per a una sucursal bancària que contingui informació sobre els clients, els comptes, les sucursals i les transaccions produïdes. Construir el model E/R tenint en compte les següents restriccions:
- Una transacció ve determinada pel seu nombre de transacció, la data i la quantitat.
- Un client pot tenir diversos comptes.
- Cada comptes pot tindre més d'un client com a titular.
- Un compte només pot pertànyer a una sucursal.
***
### ETT
Desitgem informatitzar la informació que manipula una ETT sobre empreses que ofereixen llocs de treball i persones que busquen feina. Les empreses ofereixen llocs de treball, informant de la professió sol·licitada, el lloc de treball destinat i les condicions exigides per a aquest lloc. De les persones que busquen feina tenim el seu DNI, nom, estudis i professió desitjada. Ens interessa saber quines persones poden optar a un lloc de treball, és a dir, poden participar en el procés de selecció. La persona interessada en un lloc es podrà apuntar per fer una entrevista per a aquest lloc. Per a cada lloc de treball es poden inscriure totes les persones interessades.
De cada entrevista, volem saber com ha anat, és a dir, emmagetzemar anotacions sobre si el candidat és idoni o no per al lloc de treball. En alguns casos es formalitzaran contractes entre les empreses i les persones, i emmagatzemarem la data de signatura, durada i sou del contracte. Se suposa que una persona només pot ser contractada per una empresa. de
l'empreses tenim les dades de CIF, nom i sector. A més, es distingiran pimes i multinacionals: de les primeres emmagatzemarem la ciutat en la qual s'ubica i de les segones el nombre de països en els quals té representació.
***
### Docència
Es desitja dissenyar una base de dades per a una Universitat que contingui informació sobre els alumnes, les assignatures i els professors. Construir un model E/R tenint en compte les següents restriccions:
- Una assignatura pot ser impartida per molts professors (no alhora) ja que poden existir grups. Un professor pot donar classes de moltes assignatures.
- Un alumne pot estar matriculat en moltes assignatures.
- Es necessita tenir constància de les assignatures en què està matriculat un
alumne, la nota obtinguda i el professor que l'ha qualificat.
- També cal tenir constància de les assignatures que imparteixen tots els professors (independentment de si tenen algun alumne matriculat en el seu grup).
- No hi ha assignatures amb el mateix nom.
Un alumne no pot estar matriculat en la mateixa assignatura amb dos professors diferents.
***
### Metro de Barcelona
Construir el model E/R que reflecteixi tota la informació necessària per a la gestió de les línies de metro d'una determinada ciutat. Els supòsits semàntics considerats són els següents:
- Una línia està composta per una sèrie d'estacions en un ordre determinat, sent molt important recollir la informació d'aquest ordre.
- Cada estació pertany almenys a una línia, podent pertànyer a diverses.
- Una estació mai deixa de pertànyer a una línia a la que anteriorment pertanyia (p. Ex., Portazgo, que pertany a la línia 1, mai no podrà deixar de pertànyer a aquesta línia).
- Cada estació pot tenir diversos accessos, però considerem que un accés només pot pertànyer a una estació.
- Un accés mai podrà canviar d'estació.
- Cada línia té assignats una sèrie de trens, no podent succeir que un tren estigui assignat a més d'una línia, però sí que no estigui assignat a cap (p. Ex., Si es troba en reparació).
- Algunes estacions tenen assignades cotxeres, i cada tren té assignada una cotxera.
- Interessa conèixer tots els accessos de cada línia.
***
